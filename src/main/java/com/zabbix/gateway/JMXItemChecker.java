/*
** Zabbix
** Copyright (C) 2000-2011 Zabbix SIA
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**/

package com.zabbix.gateway;

import java.util.HashMap;

import javax.management.MBeanAttributeInfo;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.TabularData;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JMXItemChecker extends ItemChecker {
	
	private static final Logger logger = LoggerFactory.getLogger(JMXItemChecker.class);
	
	public static final String JMX_KEY = "jmx";
	public static final String JMX_DISCOVERY_KEY = "jmx.discovery";

	private JMXServiceURL url;
	private JMXConnector jmxc;
	private MBeanServerConnection mbsc;

	private String username;
	private String password;

	public JMXItemChecker(JSONObject request) throws ZabbixException {
		
		super(request);

		try	{
			String conn = request.getString(JSON_TAG_CONN);
			int port = request.getInt(JSON_TAG_PORT);

			url = new JMXServiceURL("service:jmx:rmi:///jndi/rmi://" + conn + ":" + port + "/jmxrmi");
			
			jmxc = null;
			mbsc = null;

			username = request.optString(JSON_TAG_USERNAME, null);
			password = request.optString(JSON_TAG_PASSWORD, null);

			if (null != username && null == password || null == username && null != password)
				throw new IllegalArgumentException("invalid username and password nullness combination");
		}
		catch (Exception e)	{
			throw new ZabbixException(e);
		}
	}

	@Override
	public JSONArray getValues() throws ZabbixException {
		
		JSONArray values = new JSONArray();

		try
		{
			HashMap<String, String[]> env = null;

			if (null != username && null != password)
			{
				env = new HashMap<String, String[]>();
				env.put(JMXConnector.CREDENTIALS, new String[] {username, password});
			}
			
			logger.debug("connecting to JMX agent at {}", url);
			jmxc = JMXConnectorFactory.connect(url, env);
			mbsc = jmxc.getMBeanServerConnection();

			for (String key : keys)
				values.put(getJSONValue(key));
		}
		catch (Exception e)
		{
			throw new ZabbixException(e);
		}
		finally
		{
			try { if (null != jmxc) jmxc.close(); } catch (java.io.IOException exception) { }

			jmxc = null;
			mbsc = null;
		}

		return values; 
	}

	@Override
	protected String getStringValue(String key) throws Exception {
		
		ZabbixItem item = new ZabbixItem(key);

		if (item.getKeyId().equals(JMX_KEY)) {
			
			if (item.getArgumentCount() != 2)
				throw new ZabbixException("required key format: jmx[<object name>,<attribute name>]");

			ObjectName objectName = new ObjectName(item.getArgument(1));
			String attributeName = item.getArgument(2);

			StringBuilder realAttributeName = new StringBuilder();
			StringBuilder fieldNames = new StringBuilder();
			loadDataObjectNameFieldNames(realAttributeName, fieldNames, attributeName);
			
			logger.trace("attributeName:'{}'", realAttributeName);
			logger.trace("fieldNames:'{}'", fieldNames);

			return getPrimitiveAttributeValue(mbsc.getAttribute(objectName, realAttributeName.toString()), fieldNames.toString());
		}
		else if (item.getKeyId().equals(JMX_DISCOVERY_KEY)) {
			
			if (item.getArgumentCount() != 0)
				throw new ZabbixException("required key format: jmx.discovery");

			JSONArray counters = new JSONArray();

			for (ObjectName name : mbsc.queryNames(null, null))	{
				
				logger.trace("discovered object '{}'", name);

				for (MBeanAttributeInfo attrInfo : mbsc.getMBeanInfo(name).getAttributes())
				{
					logger.trace("discovered attribute '{}'", attrInfo.getName());

					if (!attrInfo.isReadable())
					{
						logger.trace("attribute not readable, skipping");
						continue;
					}

					try
					{
						logger.trace("looking for attributes of primitive types");
						String descr = (attrInfo.getName().equals(attrInfo.getDescription()) ? null : attrInfo.getDescription());
						findPrimitiveAttributes(counters, name, descr, attrInfo.getName(), mbsc.getAttribute(name, attrInfo.getName()));
					}
					catch (Exception e)
					{
						Object[] logInfo = {name, attrInfo.getName(), e};
						logger.trace("processing '{},{}' failed", logInfo);
					}
				}
			}

			JSONObject mapping = new JSONObject();
			mapping.put(ItemChecker.JSON_TAG_DATA, counters);
			return mapping.toString(2);
		}
		else {
			throw new ZabbixException("key ID '%s' is not supported", item.getKeyId());
		}
	}

	private String getPrimitiveAttributeValue(Object dataObject, String fieldNames) throws ZabbixException {
		
		logger.trace("drilling down with data object '{}' and field names '{}'", dataObject, fieldNames);

		if (null == dataObject)
			throw new ZabbixException("data object is null");
		
		if (fieldNames.isEmpty()) {
			
			if (HelperFunctionChest.isPrimitiveAttributeType(dataObject.getClass())) {
				return dataObject.toString();
			}
			else if (dataObject instanceof Object[]) {
				
				StringBuilder arrayDataObjet = new StringBuilder();
				
				for (Object objectElement : (Object[]) dataObject) 
					arrayDataObjet.append(objectElement).append(", ");
				
				return arrayDataObjet.toString().replaceFirst(",\\s$", ""); // removing last comma
			}
			else {
				throw new ZabbixException("data object type is not primitive nor array of primitive: %s" + dataObject.getClass());
			}
		}

		if (dataObject instanceof CompositeData) {
			
			logger.trace("'{}' contains composite data", dataObject);

			CompositeData comp = (CompositeData)dataObject;

			StringBuilder dataObjectName = new StringBuilder();
			StringBuilder newFieldNames = new StringBuilder();
			loadDataObjectNameFieldNames(dataObjectName, newFieldNames, fieldNames);
			
			return getPrimitiveAttributeValue(comp.get(dataObjectName.toString()), newFieldNames.toString());
		}
		else if (dataObject instanceof TabularData) {
			
			logger.trace("'{}' contains tabular data", dataObject);
			
			TabularData tab = (TabularData) dataObject;
			
			StringBuilder dataObjectName = new StringBuilder();
			StringBuilder newFieldNames = new StringBuilder();
			loadDataObjectNameFieldNames(dataObjectName, newFieldNames, fieldNames);
			Object[] keyDataObjectName = { dataObjectName.toString() };
			
			return getPrimitiveAttributeValue(tab.get(keyDataObjectName), newFieldNames.toString());
		}
		else {
			throw new ZabbixException("unsupported data object type along the path: %s", dataObject.getClass());
		}
	}
	
	
	private void findPrimitiveAttributes(JSONArray counters, ObjectName name, String descr, String attrPath, Object attribute) throws JSONException	{
		
		logger.trace("drilling down with attribute path '{}'", attrPath);

		if (HelperFunctionChest.isPrimitiveAttributeType(attribute.getClass()))	{
			
			logger.trace("found attribute of a primitive type: {}", attribute.getClass());

			JSONObject counter = new JSONObject();

			counter.put("{#JMXDESC}", null == descr ? name + "," + attrPath : descr);
			counter.put("{#JMXOBJ}", name);
			counter.put("{#JMXATTR}", attrPath);
			counter.put("{#JMXTYPE}", attribute.getClass().getName());
			counter.put("{#JMXVALUE}", attribute.toString());
 
			counters.put(counter);
		}
		else if (attribute instanceof CompositeData) {
			
			logger.trace("found attribute of a composite type: {}", attribute.getClass());

			CompositeData comp = (CompositeData) attribute;

			for (String key : comp.getCompositeType().keySet())
				findPrimitiveAttributes(counters, name, descr, attrPath + "." + key, comp.get(key));
		}
		else if (attribute instanceof TabularData || attribute.getClass().isArray()) {
			logger.trace("found attribute of a known, unsupported type: {}", attribute.getClass());
		}
		else {
			logger.trace("found attribute of an unknown, unsupported type: {}", attribute.getClass());
		}
	}
	
	
	/**
	 * Attribute name and composite data field names are separated by dots. On the other hand the
	 * name may contain a dot too. In this case user needs to escape it with a backslash. Also the
	 * backslash symbols in the name must be escaped. So a real separator is unescaped dot and
	 * separatorIndex() is used to locate it.
	 * 
	 * @param dataObjectName
	 * @param newfieldNames
	 * @param fieldNames
	 */
	private void loadDataObjectNameFieldNames(StringBuilder dataObjectName, StringBuilder newfieldNames, String fieldNames) {
		
		int sep = HelperFunctionChest.separatorIndex(fieldNames);
		String tempDataObjectName;
		
		if (sep != -1) {
			tempDataObjectName = fieldNames.substring(0, sep);
			newfieldNames.append(fieldNames.substring(sep + 1));
		}
		else
			tempDataObjectName = fieldNames;

		// Unescape possible dots or backslashes that were escaped by user
		dataObjectName.append(HelperFunctionChest.unescapeUserInput(tempDataObjectName));
	}
	
	
	public JMXServiceURL getUrl() {
		return url;
	}

	public void setUrl(JMXServiceURL url) {
		this.url = url;
	}
	
}
