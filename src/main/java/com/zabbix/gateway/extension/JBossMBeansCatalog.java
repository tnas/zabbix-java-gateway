package com.zabbix.gateway.extension;

import java.util.HashSet;
import java.util.Set;

import com.zabbix.gateway.ItemChecker;

public class JBossMBeansCatalog implements MBeansCatalog {

	private static final Class<? extends ItemChecker> itemCheckerClass = JMXJBossItemChecker.class;
	
	private static Set<String> mbeanSet = new HashSet<String>();
	
	static {
		mbeanSet.add("jboss");
		mbeanSet.add("jboss.admin");
		mbeanSet.add("jboss.alerts");
		mbeanSet.add("jboss.aop");
		mbeanSet.add("jboss.cache");
		mbeanSet.add("jboss.classloader");
		mbeanSet.add("jboss.deployment");
		mbeanSet.add("jboss.ejb");
		mbeanSet.add("jboss.j2ee");
		mbeanSet.add("jboss.jacc");
		mbeanSet.add("jboss.jca");
		mbeanSet.add("jboss.jdbc");
		mbeanSet.add("jboss.jmx");
		mbeanSet.add("jboss.management.local");
		mbeanSet.add("jboss.messaging");
		mbeanSet.add("jboss.messaging.connectionFactory");
		mbeanSet.add("jboss.messaging.destination");
		mbeanSet.add("jboss.mq");
		mbeanSet.add("jboss.pojo");
		mbeanSet.add("jboss.remoting");
		mbeanSet.add("jboss.rmi");
		mbeanSet.add("jboss.security");
		mbeanSet.add("jboss.system");
		mbeanSet.add("jboss.vfs");
		mbeanSet.add("jboss.web");
		mbeanSet.add("jboss.web.deployment");
		mbeanSet.add("jboss.ws");
	}
	
	public boolean contains(String mbean) {
		return mbeanSet.contains(mbean);
	}
	
	public Class<? extends ItemChecker> getClassItemChecker() {
		return itemCheckerClass;
	}
}
