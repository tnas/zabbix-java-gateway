package com.zabbix.gateway.extension;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.zabbix.gateway.ItemChecker;
import com.zabbix.gateway.JMXItemChecker;
import com.zabbix.gateway.ZabbixException;
import com.zabbix.gateway.ZabbixItem;

public class JMXItemCheckerFactory {
	
	private static JMXItemCheckerFactory _instance;
	
	public static JMXItemCheckerFactory getInstance() {
		if (_instance == null) _instance = new JMXItemCheckerFactory();
		return _instance;
	}
	
	private List<MBeansCatalog> mbeansCatalogList;
	private Map<MBeansCatalog, JSONObject> mapMbeansCatalogJSONObject;
	private List<ItemChecker> listItemChecker;
	
	private JMXItemCheckerFactory() {
		this.listItemChecker = new ArrayList<ItemChecker>();
		this.mbeansCatalogList = new ArrayList<MBeansCatalog>();
		this.mapMbeansCatalogJSONObject = new HashMap<MBeansCatalog, JSONObject>();
		this.loadAdditionalCatalogsItemChecker();
		this.loadDefaultCatalogItemChecker();
	}
	
	
	public List<ItemChecker> getItemChecker(JSONObject request) throws ZabbixException {

		this.reset();
		
		try {
			
			JSONArray jsonKeys = request.getJSONArray(ItemChecker.JSON_TAG_KEYS);
			
			// Checking a jmx.discovery key
			if (jsonKeys.getString(0).equals(JMXItemChecker.JMX_DISCOVERY_KEY)) {
				this.listItemChecker.add(new JMXItemChecker(request));
				return this.listItemChecker;
			}

			for (int pos = 0; pos < jsonKeys.length(); ++pos) {
				
				String jsonKey = jsonKeys.getString(pos);
				ZabbixItem keyItem = new ZabbixItem(jsonKey);
				String mbeanKey = keyItem.getArgument(1).split(":")[0];
				
				for (MBeansCatalog catalog : this.mbeansCatalogList) {
					
					if (catalog.contains(mbeanKey)) { 
						
						if (this.mapMbeansCatalogJSONObject.containsKey(catalog)) {
							this.mapMbeansCatalogJSONObject.get(catalog).getJSONArray(ItemChecker.JSON_TAG_KEYS).put(jsonKey);
						}
						else {
							JSONObject requestClone = new JSONObject(request.toString());
							requestClone.remove(ItemChecker.JSON_TAG_KEYS);
							
							JSONArray keysPerCatalog = new JSONArray();
							keysPerCatalog.put(jsonKey);
							requestClone.put(ItemChecker.JSON_TAG_KEYS, keysPerCatalog);
							
							this.mapMbeansCatalogJSONObject.put(catalog, requestClone);
						}
						
						break;
					}
				}	
			}
			
			for (MBeansCatalog catalog : this.mapMbeansCatalogJSONObject.keySet()) 
				this.listItemChecker.add(catalog.getClassItemChecker().
							getConstructor(JSONObject.class).newInstance(
									this.mapMbeansCatalogJSONObject.get(catalog)));
			
			return this.listItemChecker;
		}  
		catch (Exception e) {
			throw new ZabbixException(e);
		}
	}
	
	private void loadAdditionalCatalogsItemChecker() {
		this.mbeansCatalogList.add(new JBossMBeansCatalog());
	}
	
	private void loadDefaultCatalogItemChecker() {
		this.mbeansCatalogList.add(new JVMMBeansCatalog());
	}
	
	private void reset() {
		this.listItemChecker.clear();
		this.mapMbeansCatalogJSONObject.clear();
	}

}
