package com.zabbix.gateway.extension;

import javax.management.remote.JMXServiceURL;

import org.json.JSONObject;

import com.zabbix.gateway.JMXItemChecker;
import com.zabbix.gateway.ZabbixException;

public class JMXJBossItemChecker extends JMXItemChecker {

	private static final String JMX_URL_SERVICE_PREFIX = "service:jmx:rmi:///jndi/rmi://";
	private static final String RMI_CONNECTOR_SERVER_STUB = "/jmxconnector";
	private static final int RMI_DEFAULT_PORT = 1090;
	
	public JMXJBossItemChecker(JSONObject request) throws ZabbixException {
		
		super(request);
		
		try {
			setUrl(new JMXServiceURL(JMX_URL_SERVICE_PREFIX + request.getString(JSON_TAG_CONN) + ":" + 
					RMI_DEFAULT_PORT + RMI_CONNECTOR_SERVER_STUB));
		} 
		catch (Exception e) {
			throw new ZabbixException(e);
		} 
	}

}
