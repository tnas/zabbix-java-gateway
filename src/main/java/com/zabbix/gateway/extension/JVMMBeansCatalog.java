package com.zabbix.gateway.extension;

import com.zabbix.gateway.ItemChecker;
import com.zabbix.gateway.JMXItemChecker;

public class JVMMBeansCatalog implements MBeansCatalog {

	private static final Class<? extends ItemChecker> itemCheckerClass = JMXItemChecker.class;
	
	public boolean contains(String mbean) {
		return true;
	}

	public Class<? extends ItemChecker> getClassItemChecker() {
		return itemCheckerClass;
	}

}
