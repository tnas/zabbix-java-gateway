package com.zabbix.gateway.extension;

import com.zabbix.gateway.ItemChecker;

public interface MBeansCatalog {

	boolean contains(String mbean);
	
	Class<? extends ItemChecker> getClassItemChecker();
}
