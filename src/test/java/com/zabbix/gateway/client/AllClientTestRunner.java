package com.zabbix.gateway.client;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.zabbix.gateway.JavaGateway;

@RunWith(Suite.class)
@SuiteClasses({ JavaGatewayJBossTest.class, JavaGatewayTomcatTest.class })
public class AllClientTestRunner {
	
	private static Thread javaGateway; 
	
	@BeforeClass
	public static void setUp() throws InterruptedException {
		
		javaGateway = new Thread() {
			String[] args = { };
			public void run() {
				JavaGateway.main(args);
			}
		};
		
		javaGateway.start();
	}
	
	@AfterClass
	public static void tearDown() {
		javaGateway.interrupt();
	}

}
