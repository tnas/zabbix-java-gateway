package com.zabbix.gateway.client;

import java.io.IOException;

import org.json.JSONException;
import org.junit.Assert;
import org.junit.Test;



public class JavaGatewayJBossTest extends JavaGatewayTest {
	
	private static final String jboss_host = "10.14.1.130";
	private static final String jmx_port = "9999";
	
	@Override
	protected String getHost() {
		return jboss_host;
	}

	@Override
	protected String getJmxPort() {
		return jmx_port;
	}
	
	@Test
	public void testJbossFreeMemory() throws IOException, JSONException {
		this.requestKeysList.put("jmx[jboss.system:type=ServerInfo, FreeMemory]");	
		this.ioWithSocket();
		Assert.assertTrue(this.hasSuccessResponse);
		Assert.assertFalse(this.hasDataError);
	}
	
	@Test
	public void testJbossTotalMemory() throws IOException, JSONException {
		this.requestKeysList.put("jmx[jboss.system:type=ServerInfo,TotalMemory]");	
		this.ioWithSocket();
		Assert.assertTrue(this.hasSuccessResponse);
		Assert.assertFalse(this.hasDataError);
	}
	
	@Test
	public void testJbossWebWithJavaLangWithJbossSystem() throws IOException, JSONException {
		this.requestKeysList.put("jmx[java.lang:type=OperatingSystem, FreePhysicalMemorySize]");
		this.requestKeysList.put("jmx[\"jboss.web:type=ThreadPool,name=http-0.0.0.0-8080\",maxThreads]");
		this.requestKeysList.put("jmx[\"jboss.system:type=ServerInfo\",MaxMemory]");
		this.ioWithSocket();
		Assert.assertTrue(this.hasSuccessResponse);
		Assert.assertFalse(this.hasDataError);
	} 
	
	
	@Test
	public void testJbossThreadPool_CurrentThreadCount() throws IOException, JSONException {
		this.requestKeysList.put("jmx[\"jboss.web:type=ThreadPool,name=http-0.0.0.0-8080\",currentThreadCount]");	
		this.ioWithSocket();
		Assert.assertTrue(this.hasSuccessResponse);
		Assert.assertFalse(this.hasDataError);
	}
	
	@Test
	public void testJbossMultipleBeans() throws IOException, JSONException {
		this.requestKeysList.put("jmx[\"jboss.system:service=ThreadPool\",MaximumPoolSize]");
		this.requestKeysList.put("jmx[\"jboss.system:type=ServerInfo\",MaxMemory]");
		this.requestKeysList.put("jmx[\"jboss.system:type=ServerInfo\",TotalMemory]");
		this.requestKeysList.put("jmx[\"java.lang:type=Memory\",NonHeapMemoryUsage.used]");
		this.requestKeysList.put("jmx[\"jboss.web:type=ThreadPool,name=http-0.0.0.0-8080\",currentThreadsBusy]");
		this.requestKeysList.put("jmx[\"jboss.web:type=ThreadPool,name=http-0.0.0.0-8080\",currentThreadCount]");
		this.requestKeysList.put("jmx[\"jboss.system:type=ServerInfo\",\"ActiveThreadCount\"]");
		this.requestKeysList.put("jmx[\"jboss.system:type=ServerInfo\",FreeMemory]");
		this.requestKeysList.put("jmx[\"java.lang:type=Memory\",HeapMemoryUsage.used]");
		this.requestKeysList.put("jmx[\"java.lang:type=Memory\",NonHeapMemoryUsage.max]");
		this.requestKeysList.put("jmx[\"jboss.web:type=ThreadPool,name=http-0.0.0.0-8080\",maxThreads]");
		this.requestKeysList.put("jmx[\"java.lang:type=Memory\",HeapMemoryUsage.max]");
				
		this.ioWithSocket();
		Assert.assertTrue(this.hasSuccessResponse);
		Assert.assertFalse(this.hasDataError);
	}
}
