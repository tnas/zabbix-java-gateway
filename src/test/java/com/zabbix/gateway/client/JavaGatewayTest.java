package com.zabbix.gateway.client;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public abstract class JavaGatewayTest {

	private static final String hostName = "localhost";
	private static final int port = 10052;
	private static final byte[] protocol_header = {'Z', 'B', 'X', 'D', '\1'};
	private static final String request_header = "java gateway jmx";
	
	private Socket zabbixSocket;
	protected DataOutputStream out;
	protected BufferedReader in;
	
	protected JSONObject request;
	protected JSONArray requestKeysList;
	protected JSONObject response;
	
	protected Boolean hasDataError;
	protected Boolean hasSuccessResponse;
	
	@Before
	public void setUp() throws Exception {

		this.zabbixSocket = new Socket(hostName, port);
		this.out = new DataOutputStream(zabbixSocket.getOutputStream());
		this.in = new BufferedReader(new InputStreamReader(zabbixSocket.getInputStream()));
		
		this.request = new JSONObject();
		this.requestKeysList = new JSONArray();
		
		request.put("request", request_header);
		this.request.put("conn", getHost());
		this.request.put("port", getJmxPort());
		this.request.put("keys", this.requestKeysList);
		
		this.response = new JSONObject();
		
		this.hasDataError = null;
		this.hasSuccessResponse = null;
	}
	
	@After
	public void tearDown() throws Exception {		
		zabbixSocket.close();
		out.close();
		in.close();
	}
	
	
	protected void ioWithSocket() throws IOException, JSONException {
				
		ByteBuffer bb = ByteBuffer.allocate(8);
		bb.order(ByteOrder.LITTLE_ENDIAN);
		bb.asLongBuffer().put(request.toString().length());
		bb.rewind();

		// Writing request into socket
		out.write(protocol_header);
		out.write(bb.array());
		out.writeBytes(request.toString());
		
		// Reading response from stdin
		StringBuilder responseBuilder = new StringBuilder();
		String responseLine;
		while ((responseLine = in.readLine()) != null) 
			responseBuilder.append(responseLine);
		//System.out.println(responseBuilder.toString());
		
		this.response = new JSONObject(responseBuilder.toString().replaceFirst("^.*?\\{", "{").trim());
		
		String responseValue = (String) this.response.get("response");
		this.hasSuccessResponse = responseValue != null && responseValue.equals("success");
		
		JSONArray dataArray = (JSONArray) this.response.get("data");
		this.hasDataError = false;
		for (int i = 0; i < dataArray.length(); ++i) {
			if (dataArray.getJSONObject(i).has("error")) {
				this.hasDataError = true;
				break;
			}
		}
	}
	
	protected abstract String getHost();
	
	protected abstract String getJmxPort();
	
	
	@Test
	public void testJMXDiscovery() throws IOException, JSONException {
		this.requestKeysList.put("jmx.discovery");
		this.ioWithSocket();
		Assert.assertTrue(this.hasSuccessResponse);
		Assert.assertFalse(this.hasDataError);
	}
	
	@Test
	public void testJavaLang_OperatinSystem_FreePhysicalMemorySize() throws IOException, JSONException {
		this.requestKeysList.put("jmx[java.lang:type=OperatingSystem, FreePhysicalMemorySize]");
		this.ioWithSocket();
		Assert.assertTrue(this.hasSuccessResponse);
		Assert.assertFalse(this.hasDataError);
	}
	
	@Test
	public void testJavaLang_Memory_HeapMemoryUsage_NonHeapMemoryUsage() throws IOException, JSONException {
		this.requestKeysList.put("jmx[java.lang:type=Memory, HeapMemoryUsage.max]");
		this.requestKeysList.put("jmx[java.lang:type=Memory, HeapMemoryUsage.used]");	
		this.requestKeysList.put("jmx[java.lang:type=Memory, NonHeapMemoryUsage.max]");
		this.requestKeysList.put("jmx[java.lang:type=Memory, NonHeapMemoryUsage.used]");
		this.ioWithSocket();
		Assert.assertTrue(this.hasSuccessResponse);
		Assert.assertFalse(this.hasDataError);
	}
	
	@Test
	public void testJavaLang_Runtime_StringArray_InputArguments() throws IOException, JSONException {
		this.requestKeysList.put("jmx[java.lang:type=Runtime, InputArguments]");
		this.ioWithSocket();
		Assert.assertTrue(this.hasSuccessResponse);
		Assert.assertFalse(this.hasDataError);
	}

	@Test
	public void testJavaLang_GarbageCollector_Copy_LastGcInfo() throws IOException, JSONException {
		this.requestKeysList.put("jmx[\"java.lang:type=GarbageCollector,name=PS MarkSweep\", LastGcInfo.memoryUsageAfterGc.PS Perm Gen.value.used]");
		this.ioWithSocket();
		Assert.assertTrue(this.hasSuccessResponse);
		Assert.assertFalse(this.hasDataError);
	}
}
