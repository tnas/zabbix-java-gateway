package com.zabbix.gateway.client;

import java.io.IOException;

import org.json.JSONException;
import org.junit.Assert;
import org.junit.Test;

public class JavaGatewayTomcatTest extends JavaGatewayTest {

	private static final String jboss_host = "10.14.1.118";
	private static final String jmx_port = "9999";

	
	@Override
	protected String getHost() {
		return jboss_host;
	}

	@Override
	protected String getJmxPort() {
		return jmx_port;
	}

	@Test
	public void testTomcat_Server_ServerInfo() throws IOException, JSONException {
		this.requestKeysList.put("jmx[Catalina:type=Server, serverInfo]");	
		this.ioWithSocket();
		Assert.assertTrue(this.hasSuccessResponse);
		Assert.assertFalse(this.hasDataError);
	}

	
	@Test
	public void testTomcat_Server_ServiceNames() throws IOException, JSONException {
		this.requestKeysList.put("jmx[Catalina:type=Server, serviceNames]");	
		this.ioWithSocket();
		Assert.assertTrue(this.hasSuccessResponse);
		Assert.assertFalse(this.hasDataError);
	}
	
}
