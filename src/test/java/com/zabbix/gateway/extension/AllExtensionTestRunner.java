package com.zabbix.gateway.extension;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ JMXItemCheckerFactoryTest.class })
public class AllExtensionTestRunner {

}
