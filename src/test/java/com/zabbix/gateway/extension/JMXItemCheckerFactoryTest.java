package com.zabbix.gateway.extension;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import com.zabbix.gateway.ItemChecker;
import com.zabbix.gateway.JMXItemChecker;
import com.zabbix.gateway.ZabbixException;

public class JMXItemCheckerFactoryTest {

	private static final String HOST = "localhost";
	private static final int JMX_PORT = 9999;
	private static final String request_header = "java gateway jmx";
	
	private JSONObject request;
	private JSONArray keys;
	private JMXItemCheckerFactory factory;
	
	public JMXItemCheckerFactoryTest() {
		this.factory = JMXItemCheckerFactory.getInstance();
	}
	
	
	@Before
	public void setUp() throws JSONException {
		this.request = new JSONObject();
		this.keys = new JSONArray();
		this.request.put("request", request_header);
		this.request.put("conn", HOST);
		this.request.put("port", JMX_PORT);
		this.request.put("keys", keys);
	}
	
	
	@Test
	public void testGetJBossItemChecker() throws JSONException, ZabbixException {
		this.keys.put("jmx[jboss.system:type=ServerInfo, FreeMemory]");
		List<ItemChecker> listItemChecker = this.factory.getItemChecker(this.request);
		assertEquals(1, listItemChecker.size());
		assertTrue(listItemChecker.get(0) instanceof JMXJBossItemChecker);
	}
	
	
	@Test
	public void testGetJVMItemChecker() throws JSONException, ZabbixException {
		this.keys.put("jmx[java.lang:type=OperatingSystem, FreePhysicalMemorySize]");
		List<ItemChecker> listItemChecker = this.factory.getItemChecker(this.request);
		assertEquals(1, listItemChecker.size());
		assertTrue(listItemChecker.get(0) instanceof JMXItemChecker);
	}
	
	
	@Test
	public void testGetJVMandJBossItemChecker() throws JSONException, ZabbixException {
		this.keys.put("jmx[java.lang:type=OperatingSystem, FreePhysicalMemorySize]");
		this.keys.put("jmx[jboss.system:type=ServerInfo, FreeMemory]");
		List<ItemChecker> listItemChecker = this.factory.getItemChecker(this.request);
		assertEquals(2, listItemChecker.size());
		assertTrue(listItemChecker.get(0) instanceof JMXJBossItemChecker || 
				listItemChecker.get(1) instanceof JMXJBossItemChecker);
	}
}
